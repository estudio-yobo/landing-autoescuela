module.exports = {
  purge: ["./src/**/*.js"],
  target: "relaxed",
  theme: {
    extend: {
      colors: {
        green: {
          100: "#f3f7e8",
          200: "#e1ecc4",
          300: "#cfe0a1",
          400: "#acc95b",
          500: "#88b214",
          600: "#7aa012",
          700: "#66860f",
          800: "#526b0c",
          900: "#43570a",
        },
      },
    },
  },
  variants: {},
  plugins: [],
}
