const resolveConfig = require("tailwindcss/resolveConfig")
const tailwindConfig = require("./tailwind.config.js")

const { theme } = resolveConfig(tailwindConfig)

module.exports = {
  siteMetadata: {
    title: "Recupera Tus Puntos",
    author: "Estudio YOBO",
    description: "Web oficial de la app Recupera Tus Puntos",
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        name: "Recupera Tus Puntos",
        short_name: "Recupera Tus Puntos",
        start_url: "/",
        background_color: theme.colors.white,
        theme_color: theme.colors.green[500],
        icon: "static/icon.png",
      },
    },
    {
      resolve: "gatsby-plugin-postcss",
      options: {
        postCssPlugins: [require("tailwindcss"), require("autoprefixer")],
      },
    },
  ],
}
